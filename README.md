--Port Scanner--

✔ 1. Write a basic port scanner with a timeout after attempt to connect after (x) seconds.

        Script will look something like such (this example excludes the timeout):
        self.s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.s.connect((ip_goes_here, port_goes_here))

✔ 2. Write a URL to IP converter function

✔ 3. Grab banner from each port if it's open using recv

✔ 4. Write a function that will print if a banner is returned on a port and searches the returned banner through the vulners database library (example at the beginning of channel), if there is a vulnerability/exploit print it as well.

✔ 5. Be sure to use a class for your script (and utilize self for all attributes) so we can import and use it elsewhere as needed.

6. If port 80 is found, pass it to the CMS Scanner code through an imported module.

--CMS Scanner--
1. Find a way to discover if a target is utilizing a CMS, and if so what CMS that target is running.
        The easiest method I've read about is to pull a pages source and search it for the name of the CMS. We can use newyorker.com to test, it is using wordpress.
2. Search the version/CMS type for vulnerabilities/exploits on vulners database library, print results.
3. Discover plugins within the CMS, and search each through vulners database library, print results.
4. Scrape the CMS directory for admin login page. We'll probably have to find a good list of possible login/admin page names to look for.
5. Be sure to use a class for your script (and utilize self for all attributes) so we can import and use it elsewhere as needed.
I'll probably spend a good part of the day researching more on the topic and leaving anything decent here as well.
https://www.securitynewspaper.com/2019/12/16/top-python-libraries-used-in-hacking/