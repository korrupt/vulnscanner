import socket
import vulners
from threading import Thread
import builtwith
from urllib import request
import pycurl
from bs4 import BeautifulSoup
from googlesearch import search 


class portScan:
    def __init__(self):
        ### Put your global variables here
        ### Use http://testphp.vulnweb.com/ to test
        self.vulners_api = vulners.Vulners(api_key="WX4PTUG04464XNAIC8OLVIF1MY4PNR5ZL3J6F3IMYPM92HIL5EG36RSRSCPLPKY6")
    
    def main(self):
        ## User input area
        self.ip = input("Enter a target: ")
        self.ports = input("How many ports would you like to scan: ")
        self.timeout = float(input("How many seconds would you like to wait per port: "))
        ## Add 1 to the port amount (because the scan will start at 0)
        self.ports = int(self.ports)+1
        print("Working...")
        self.checks(self)
        
    def checks(self):
        try:
            ## Fixing input if http:// is entered
            if self.ip[:7] == "http://":
                ## Removing http:// from the string
                self.ip = self.ip[7:]
            ## Same as above, but for https://
            elif self.ip[:8] == "https://":
                self.ip = self.ip[8:]
            ## Setting self.domain so that the cms scanner can utilize it later
            self.domain = "http://" + self.ip
            ## Getting IP if a domain name is entered
            self.newip = socket.gethostbyname(str(self.ip))
            ## Printing the conversion
            print("%s = %s" % (self.ip, self.newip))
            self.ip = self.newip
        except:
            pass
        finally:
            ## Create a loop for the scan, this is done here so we can utilize threading for the scan.
            for i in range(self.ports):
                ## Thread each port for a faster scan.
                Thread(target=self.scan, args=(self, str(i))).start()
        
        
    def scan(self, port):
            ## The actual scan
        #for i in range(self.ports):
        try:
            self.s = socket.socket()
            self.s.settimeout(self.timeout)
            self.s.connect((self.ip, int(port)))
            try:
                ## If the port is opened this will display
                print("+ Port Open: %s" % port)
                ## Change the timeout to give the banner time to be received
                self.s.settimeout(10)
                if int(port) == 80:
                    Thread(target=self.cms(self)).start()
                ## Check for a banner
                banner = self.s.recv(3072).decode('utf-8')
                ## If the banner is received it will print
                print("+ Banner Found: %s" % str(banner))
                try:
                    ## If the banner is found it will look here for exploits
                    self.exploit = self.vulners_api.searchExploit(str(banner.strip('\n')))
                    ## Print the exploit if it's found
                    print("+ Exploit Found: %s" % self.exploit)
                    ## Search exploit DB for banner (may need to parse the banner for better restults, we will have to experiment.)
                    ContentCallback.exploitdb_search(str(banner.strip('\n')))
                except:
                    ## Nothing found
                    print("No exploits found.")
            except:
                pass
                
        except:
            #print(i)
            pass
            
    def cms(self):
        print("a")
        self.build = builtwith.builtwith('http://' + self.domain)
        print(self.build)
        for i in self.build:
            self.exploit = self.vulners_api.searchExploit(str(i))
            print(self.exploit)
        
        
        
        
        
class ContentCallback:
    def __init__(self):
        self.contents = ''

    def content_callback(self, buf):
        self.contents = self.contents + str(buf)


    def exploitdb_search(name):
        if len(name) != 0:
            try:
                query = str(name) + ' ' + 'site:https://www.exploit-db.com'
                for data in  search(query, tld="com", num=20, start=0, stop=25, pause=2):
                    if "https://www.exploit-db.com/exploits" in data:
                        t = ContentCallback()
                        curlObj = pycurl.Curl()
                        curlObj.setopt(curlObj.URL, '{}'.format(data))
                        curlObj.setopt(curlObj.WRITEFUNCTION, t.content_callback)
                        curlObj.perform()
                        curlObj.close()
                        print("Url:" + ' ' + data) 
                        soup = BeautifulSoup(t.contents,'lxml')
                        desc = soup.find("meta", property="og:title")
                        print("Title:" +  ' ' + desc["content"] if desc else "Cannot find the description for the exploit")
                        author = soup.find("meta", property="article:author")
                        print("Author:" + ' ' +  author["content"] if author else "No author name found")
                        publish = soup.find("meta", property="article:published_time")
                        print("Publish Date:" +  ' ' + publish["content"] if publish else "Cannot find the published date")
                        print

            except:
                print("Connection Error!")    
            

            
portScan.main(portScan)


